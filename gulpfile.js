const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');

sass.compiler = require('node-sass');

const onError = function (err) {
    notify.onError({message: 'Error: <%= error.message %>'})(err);
    this.emit('end');
};

gulp.task('clean', function () {
    return gulp.src('dist', {read: false, allowEmpty: true})
        .pipe(plumber({errorHandler: onError}))
        .pipe(clean());
});

gulp.task('html', function () {
    return gulp.src('./src/**/*.html')
        .pipe(plumber({errorHandler: onError}))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('styles', function () {
    return gulp.src('./src/**/styles/index.scss')//Zzzzz/
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./dist'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('images', function () {
    return gulp.src('./src/**/images/*.*')
        .pipe(plumber({errorHandler: onError}))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('build', gulp.series('clean', gulp.parallel('html', 'styles', 'images')));

gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    });
});

gulp.task('serve',
    gulp.series(
        'build',
        gulp.parallel(
            'browser-sync',
            function watch() {
                gulp.watch("src/**/styles/*.scss", gulp.series('styles'));
                gulp.watch("src/**/*.html", gulp.series('html'));
                gulp.watch('src/**/images/*.*', gulp.series('images'));
            }
        )
    )
);

gulp.task('default', gulp.series('serve'));